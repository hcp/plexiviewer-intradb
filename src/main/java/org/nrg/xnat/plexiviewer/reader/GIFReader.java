/*
 * org.nrg.xnat.plexiviewer.reader.GIFReader
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.plexiviewer.reader;

import ij.io.FileInfo;
import org.nrg.xnat.plexiviewer.utils.FileUtils;

import java.io.File;
import java.io.IOException;

public class GIFReader extends ImageReaderPreparer implements ImageReader {
    String directory, fileName;

    public GIFReader(String directory, String fileName) {
        this.directory = directory;
        this.fileName = fileName;
    }

    public boolean isZipped() {
        return zipped;
    }


    public void clearTempFolder() {
        if (zipped) {
            FileUtils.deleteFile(directory, true);
        }
    }

    public FileInfo getFileInfo() throws IOException {
        FileInfo fi   = null;
        String   path = "";

        String name = "";
        if (fileName.endsWith(".gif")) {
            name = fileName.substring(0, fileName.length() - 4);
        } else if (fileName.endsWith(".GIF")) {
            name = fileName.substring(0, fileName.length() - 4);
        } else {
            name = fileName;
        }

        String destDir = unzip(directory, fileName);
        if (zipped) {
            directory = destDir;
        }
        if (directory != null && !directory.equals("")) {
            if (!directory.endsWith("" + File.separatorChar)) {
                directory += File.separatorChar;
            }
        } else {
            System.out.println("GIFReader:: Directory string is empty or null\n");
            throw new IOException();
        }
        fi = new FileInfo();
        fi.fileFormat = fi.GIF_OR_JPG;
        fi.fileName = name + ".gif";
        fi.directory = directory;
        return fi;
    }

    public String getOrientation() {
        return null;
    }

    public int getOrientationForWriter() {
        return -1;
    }

    public int getVolumes() {
        return 0;
    }

}
