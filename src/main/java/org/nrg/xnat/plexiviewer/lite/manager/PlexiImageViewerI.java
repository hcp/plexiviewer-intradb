/*
 * org.nrg.xnat.plexiviewer.lite.manager.PlexiImageViewerI
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.plexiviewer.lite.manager;

import ij.ImagePlus;
import ij.ImageStack;
import org.nrg.xnat.plexiviewer.lite.UserSelection;
import org.nrg.xnat.plexiviewer.lite.display.MontageDisplay;
import org.nrg.xnat.plexiviewer.lite.gui.PlexiMessagePanel;
import org.nrg.xnat.plexiviewer.lite.io.PlexiImageFile;
import org.nrg.xnat.plexiviewer.lite.xml.Layout;

import java.awt.*;

public interface PlexiImageViewerI {


    public int open(PlexiMessagePanel msgPanel);

    public void handleOutOfMemoryError();

    public void setWindowDimensions(PlexiImageFile pf);

    public void setWindowDimensions(int w, int h, int s);

    public UserSelection getUserSelection();

    public ImagePlus getImagePlus();

    public Layout getLayout();

    public void adjustCoords(int x, int y, int z, float ipvalue, boolean slice, String fromView, boolean fromRadiologic);

    public void adjustRegions(int x, int y, int z, float ipvalue, boolean slice, String fromView, boolean fromRadiologic, int region);

    public void setWaitCursor(boolean status);

    public void setMessage(String msg, String desc);

    public void setMontageDisplay(MontageDisplay display);

    public Rectangle getWindowDimensions();

    public boolean isInitialized();

    public void setImage(int index, ImageStack s);

    public void setImage(int index, ImagePlus s);

    public void updateCrossHair();

    public void updateCrossHair(String orientation, int sliceNo);
}
