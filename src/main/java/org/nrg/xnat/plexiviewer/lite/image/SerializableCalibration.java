/*
 * org.nrg.xnat.plexiviewer.lite.image.SerializableCalibration
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.plexiviewer.lite.image;

import ij.measure.Calibration;

import java.io.Serializable;

public class SerializableCalibration implements Serializable {
    /**
     * Pixel width in 'unit's
     */
    public double pixelWidth = 1.0;

    /**
     * Pixel height in 'unit's
     */
    public double pixelHeight = 1.0;

    /**
     * Pixel depth in 'unit's
     */
    public double pixelDepth = 1.0;

    public SerializableCalibration() {
    }

    public SerializableCalibration(Calibration c) {
        this.pixelDepth = c.pixelDepth;
        this.pixelHeight = c.pixelHeight;
        this.pixelWidth = c.pixelWidth;
    }

    public Calibration getCalibration() {
        Calibration c = new Calibration();
        c.pixelHeight = this.pixelHeight;
        c.pixelWidth = this.pixelWidth;
        c.pixelDepth = this.pixelDepth;
        return c;
    }

    public String toString() {
        String str = "SerializableCalibration:: \n";
        str += "Pixel Width:  " + this.pixelWidth + "\n";
        str += "Pixel Height: " + this.pixelHeight + "\n";
        str += "Pixel Depth: " + this.pixelDepth + "\n";
        return str;
    }

}
