/*
 * org.nrg.xnat.plexiviewer.lite.display.Point3d
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */


package org.nrg.xnat.plexiviewer.lite.display;

import java.io.Serializable;

public class Point3d implements Serializable, Cloneable {

    private float _x;
    private float _y;
    private float _z;

    public Point3d() {
        _x = 0;
        _y = 0;
        _z = 0;
    }

    public Point3d(float x, float y, float z) {
        _x = x;
        _y = y;
        _z = z;
    }

    public Point3d(Point3d p) {
        _x = p.getX();
        _y = p.getY();
        _z = p.getZ();
    }

    public void set(Point3d p) {
        _x = p.getX();
        _y = p.getY();
        _z = p.getZ();
    }

    public void set(float x, float y, float z) {
        _x = x;
        _y = y;
        _z = z;
    }

    public String toString() {

        return " x: " + _x + " y: " + _y + " z: " + _z;
    }


    /**
     * @return The X coordinate.
     */
    public float getX() {
        return _x;
    }

    /**
     * @return The Y coordinate.
     */
    public float getY() {
        return _y;
    }

    /**
     * @return The Z coordinate.
     */
    public float getZ() {
        return _z;
    }

    public boolean equals(Point3d other) {
        boolean rtn = false;
        if ((this.getX() == other.getX()) && (this.getY() == other.getY()) && (this.getZ() == other.getZ())) {
            rtn = true;
        }
        return rtn;
    }

    /**
     * @param x The X coordinate.
     */
    public void setX(float x) {
        _x = x;
    }

    /**
     * @param y    The Y coordinate.
     */
    public void setY(float y) {
        _y = y;
    }

    /**
     * @param z    The Z coordinate.
     */
    public void setZ(float z) {
        _z = z;
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }
}

