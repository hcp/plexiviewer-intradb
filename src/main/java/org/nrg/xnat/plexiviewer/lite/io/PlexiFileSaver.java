/*
 * org.nrg.xnat.plexiviewer.lite.io.PlexiFileSaver
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */
package org.nrg.xnat.plexiviewer.lite.io;

import ij.ImagePlus;
import org.apache.commons.lang3.StringUtils;
import org.nrg.xnat.plexiviewer.ij.GifEncoder;
import org.nrg.xnat.plexiviewer.lite.gui.PlexiSaveDialog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Paths;

public class PlexiFileSaver {
    private String         name;
    private java.awt.Image image;


    public PlexiFileSaver(java.awt.Image img) {
        image = img;
    }

    public PlexiFileSaver() {
    }

    public void setImageFileName(String n) {
        name = n;
    }

    String getPath(String type, String extension) {
        final PlexiSaveDialog saveDialog = new PlexiSaveDialog("Save as " + type, name, extension);
        name = saveDialog.getFileName();
        if (StringUtils.isBlank(name)) {
            return null;
        }
        return Paths.get(saveDialog.getDirectory(), name).toString();
    }

    /**
     * Save the image in GIF format using a save file
     * dialog. Returns false if the user selects cancel
     * or the image is not 8-bits.
     */
    public boolean saveImageAsGif() {
        String path = getPath("GIF", ".gif");
        return path != null && image != null && saveImageAsGif(path);
    }

    public void saveAsJpeg(ImagePlus imp, String path) {
        saveAsMode(imp, path, "jpg");
    }

    public void saveAsPng(final ImagePlus imp, String path) {
        saveAsMode(imp, path, "png");
    }

    /**
     * Save the image in Gif format using the specified path. Returns
     * false if the image is not 8-bits or there is an I/O error.
     */
    public boolean saveImageAsGif(String path) {
        OutputStream output = null;
        boolean      rtn    = false;
        try {
            output = new BufferedOutputStream(new FileOutputStream(path));
            GifEncoder gifE = new GifEncoder(image);
            gifE.write(output);
            rtn = true;
        } catch (IOException e) {
            _log.error("An error occurred writing to " + path, e);
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        }
        return rtn;
    }

    private void saveAsMode(final ImagePlus imp, final String path, final String mode) {
        final int           width  = imp.getWidth();
        final int           height = imp.getHeight();
        final BufferedImage image  = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        try (final FileOutputStream f = new FileOutputStream(path)) {
            final Graphics g = image.createGraphics();
            g.drawImage(imp.getImage(), 0, 0, null);
            g.dispose();
            ImageIO.write(image, mode, f);
        } catch (IOException e) {
            _log.error("An error occurred writing an image to the file " + path, e);
        }
    }

    private static final Logger _log = LoggerFactory.getLogger(PlexiFileSaver.class);
}
